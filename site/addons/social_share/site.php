<?php
/**
 * @author       JoomWorker
 * @email        info@joomla.work
 * @url          http://www.joomla.work
 * @copyright    Copyright (c) 2010 - 2019 JoomWorker
 * @license      GNU General Public License version 2 or later
 * @date         2019/01/01 09:30
 */
//no direct accees
defined('_JEXEC') or die ('Restricted access');

class JwpagefactoryAddonSocial_share extends JwpagefactoryAddons
{

	public function render()
	{

		$getUri = JFactory::getURI();
		$doc = JFactory::getDocument();

		// Options
		$settings = $this->addon->settings;
		$class = (isset($settings->class) && $settings->class) ? ' ' . $settings->class : '';
		$class .= (isset($settings->social_style) && $settings->social_style) ? ' jwpf-social-share-style-' . str_replace('_', '-', $settings->social_style) : '';
		$heading_selector = (isset($settings->heading_selector) && $settings->heading_selector) ? $settings->heading_selector : 'h3';
		$title = (isset($settings->title) && $settings->title) ? $settings->title : '';
		$show_social_names = (isset($settings->show_social_names) && $settings->show_social_names) ? $settings->show_social_names : '';
		$show_counter = (isset($settings->show_counter) && $settings->show_counter) ? $settings->show_counter : '';
		$show_totalshare = (isset($settings->show_totalshare) && $settings->show_totalshare) ? $settings->show_totalshare : '';
		$show_socials = (isset($settings->show_socials) && $settings->show_socials) ? $settings->show_socials : '';
		$api = (isset($settings->api) && $settings->api) ? $settings->api : '';

		$current_url = $getUri->toString();
		$page_title = $doc->getTitle();

		$statistics = '';
		if ($show_counter || $show_totalshare) {
			if (!$api) {
				$output = '<p class="alert alert-warning">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_API_NOT_FOUND') . '</p>';
				return $output;
			}

			// Get Statistics of share
			$statistics = $this->getStatistics();
		}

		if ($show_totalshare) {
			$share_col = 'jwpf-col-sm-3';
			$icons_col = 'jwpf-col-sm-9';
		} else {
			$share_col = 'jwpf-col-sm-12';
			$icons_col = 'jwpf-col-sm-12';
		}

		$output = '';
		$output .= '<div class="jwpf-addon jwpf-addon-social-share' . $class . '">';
		$output .= '<div class="jwpf-social-share">';
		$output .= ($title) ? '<' . $heading_selector . ' class="jwpf-addon-title">' . $title . '</' . $heading_selector . '>' : '';

		$output .= '<div class="jwpf-social-share-wrap jwpf-row">';

		if (isset($statistics->total) && $show_totalshare) {
			$output .= '<div class="jwpf-social-total-shares ' . $share_col . '">';
			$output .= '<em>' . $statistics->total . '</em>';
			$output .= '<div class="jwpf-social-total-share-caption">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_TOTAL_SHARES') . '</div>';
			$output .= '</div>';
		}

		$output .= '<div class="jwpf-social-items-wrap ' . $icons_col . '">';
		$output .= '<ul>';
		if (in_array('facebook', $show_socials)) {
			$output .= '<li class="jwpf-social-share-facebook">';
			$output .= '<a onClick="window.open(\'http://www.facebook.com/sharer.php?u=' . $current_url . '\',\'Facebook\',\'width=600,height=300,left=\'+(screen.availWidth/2-300)+\',top=\'+(screen.availHeight/2-150)+\'\'); return false;" href="http://www.facebook.com/sharer.php?u=' . $current_url . '">';
			$output .= '<i class="fa fa-facebook"></i>';
			if ($show_social_names) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_FACEBOOK') . '</span>';
			}
			if (isset($statistics->shares->facebook) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->facebook . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('twitter', $show_socials)) {
			//twitter
			$output .= '<li class="jwpf-social-share-twitter">';
			$output .= '<a onClick="window.open(\'http://twitter.com/share?url=' . urlencode($current_url) . '&amp;text=' . str_replace(" ", "%20", $page_title) . '\',\'Twitter share\',\'width=600,height=300,left=\'+(screen.availWidth/2-300)+\',top=\'+(screen.availHeight/2-150)+\'\'); return false;" href="http://twitter.com/share?url=' . $current_url . '&amp;text=' . str_replace(" ", "%20", $page_title) . '">';
			$output .= '<i class="fa fa-twitter"></i>';
			if ($show_social_names) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_TWITTER') . '</span>';
			}
			if (isset($statistics->shares->twitter) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->twitter . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('gplus', $show_socials)) {
			//google plus
			$output .= '<li class="jwpf-social-share-glpus">';
			$output .= '<a onClick="window.open(\'https://plus.google.com/share?url=' . $current_url . '\',\'Google plus\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="https://plus.google.com/share?url=' . $current_url . '" >';
			$output .= '<i class="fa fa-google-plus"></i>';
			if ($show_social_names) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_GOOGLE_PLUS') . '</span>';
			}
			if (isset($statistics->shares->google) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->google . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('linkedin', $show_socials)) {
			//linkedin
			$output .= '<li class="jwpf-social-share-linkedin">';
			$output .= '<a onClick="window.open(\'http://www.linkedin.com/shareArticle?mini=true&url=' . $current_url . '\',\'Linkedin\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=' . $current_url . '" >';
			$output .= '<i class="fa fa-linkedin-square"></i>';
			if ($show_social_names) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_LINKEDIN') . '</span>';
			}
			if (isset($statistics->shares->linkedin) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->linkedin . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('pinterest', $show_socials)) {
			$output .= '<li class="jwpf-social-share-pinterest">';
			$output .= '<a onClick="window.open(\'http://pinterest.com/pin/create/button/?url=' . $current_url . '&amp;description=' . $page_title . '\',\'Pinterest\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://pinterest.com/pin/create/button/?url=' . $current_url . '&amp;description=' . $page_title . '" >';
			$output .= '<i class="fa fa-pinterest"></i>';
			if ($show_social_names == 1) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_PINTEREST') . '</span>';
			}
			if (isset($statistics->shares->pinterest) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->pinterest . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('thumblr', $show_socials)) {
			$output .= '<li class="jwpf-social-share-thumblr">';
			$output .= '<a onClick="window.open(\'http://tumblr.com/share?s=&amp;v=3&amp;t=' . $page_title . '&amp;u=' . $current_url . '\',\'Thumblr\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://tumblr.com/share?s=&amp;v=3&amp;t=' . $page_title . '&amp;u=' . $current_url . '" >';
			$output .= '<i class="fa fa-tumblr"></i>';
			if ($show_social_names == 1) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_THUMBLR') . '</span>';
			}
			if (isset($statistics->shares->tumblr) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->tumblr . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('getpocket', $show_socials)) {
			$output .= '<li class="jwpf-social-share-getpocket">';
			$output .= '<a onClick="window.open(\'https://getpocket.com/save?url=' . $current_url . '\',\'Getpocket\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="https://getpocket.com/save?url=' . $current_url . '" >';
			$output .= '<i class="fa fa-get-pocket"></i>';
			if ($show_social_names == 1) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_GETPOCKET') . '</span>';
			}
			if (isset($statistics->shares->pocket) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->pocket . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('reddit', $show_socials)) {
			$output .= '<li class="jwpf-social-share-reddit">';
			$output .= '<a onClick="window.open(\'http://www.reddit.com/submit?url=' . $current_url . '\',\'Reddit\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://www.reddit.com/submit?url=' . $current_url . '" >';
			$output .= '<i class="fa fa-reddit"></i>';
			if ($show_social_names == 1) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_REDDIT') . '</span>';
			}
			if (isset($statistics->shares->reddit) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->reddit . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('vk', $show_socials)) {
			$output .= '<li class="jwpf-social-share-vk">';
			$output .= '<a onClick="window.open(\'http://vk.com/share.php?url=' . $current_url . '\',\'Vk\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://vk.com/share.php?url=' . $current_url . '" >';
			$output .= '<i class="fa fa-vk"></i>';
			if ($show_social_names == 1) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_VK') . '</span>';
			}
			if (isset($statistics->shares->vk) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->vk . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('xing', $show_socials)) {
			$output .= '<li class="jwpf-social-share-xing">';
			$output .= '<a onClick="window.open(\'https://www.xing.com/spi/shares/new?cb=0&url=' . $current_url . '\',\'Xing\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="https://www.xing.com/spi/shares/new?cb=0&url=' . $current_url . '" >';
			$output .= '<i class="fa fa-xing"></i>';
			if ($show_social_names == 1) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_XING') . '</span>';
			}
			if (isset($statistics->shares->xing) && $statistics->shares && $show_counter) {
				$output .= '<span class="jwpf-social-share-count">' . $statistics->shares->xing . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		if (in_array('whatsapp', $show_socials)) {
			$output .= '<li class="jwpf-social-share-whatsapp">';
			$output .= '<a href="whatsapp://send?text=' . $current_url . '" >';
			$output .= '<i class="fa fa-whatsapp"></i>';
			if ($show_social_names == 1) {
				$output .= '<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_WHATSAPP') . '</span>';
			}
			$output .= '</a>';
			$output .= '</li>';
		}
		$output .= '</ul>';
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';

		return $output;
	}

	public function css()
	{
		$addon_id = '#jwpf-addon-' . $this->addon->id;

		$settings = $this->addon->settings;
		$social_use_border = (isset($settings->social_use_border) && $settings->social_use_border) ? $settings->social_use_border : '';
		$social_style = (isset($settings->social_style) && $settings->social_style) ? $settings->social_style : '';

		$style = (isset($settings->background_color) && $settings->background_color && $social_style == 'custom') ? 'background-color:' . $settings->background_color . ';' : '';
		$style .= (isset($settings->social_border_width) && $settings->social_border_width && $social_use_border) ? "border-style: solid; border-width: " . $settings->social_border_width . "px;" : '';
		$style .= (isset($settings->social_border_color) && $settings->social_border_color && $social_use_border) ? "border-color: " . $settings->social_border_color . ";" : '';
		$style .= (isset($settings->social_border_radius) && $settings->social_border_radius) ? "border-radius: " . $settings->social_border_radius . ";" : '';
		$hover_style = (isset($settings->background_hover_color) && $settings->background_hover_color && $social_style == 'custom') ? 'background-color:' . $settings->background_hover_color . ';' : '';
		$hover_style .= (isset($settings->social_border_hover_color) && $settings->social_border_hover_color && $social_use_border) ? 'border-color:' . $settings->social_border_hover_color . ';' : '';

		$css = '';
		if ($style) {
			$css .= $addon_id . ' .jwpf-social-share-wrap ul li a {' . $style . '}';
		}

		if ($hover_style) {
			$css .= $addon_id . ' .jwpf-social-share-wrap ul li a:hover {' . $hover_style . '}';
		}

		if (isset($settings->icon_align) && $settings->icon_align) {
			$css .= $addon_id . ' .jwpf-social-share-wrap {text-align:' . $settings->icon_align . ';}';
		}

		return $css;

	}

	private function getStatistics()
	{
		$getUri = JFactory::getURI();
		$current_url = $getUri->toString();

		$settings = $this->addon->settings;
		$host = (isset($settings->host) && $settings->host) ? $settings->host : 'free.donreach.com';
		$api = (isset($settings->api) && $settings->api) ? $settings->api : '';

		jimport('joomla.filesystem.folder');
		$cache_path = JPATH_CACHE . '/com_jwpagefactory/addons/addon-' . $this->addon->id;
		$cache_file = $cache_path . '/social_share.json';

		if (!file_exists($cache_path)) {
			JFolder::create($cache_path, 0755);
		}

		if (file_exists($cache_file) && (filemtime($cache_file) > (time() - 60 * 30))) {
			$statistics = file_get_contents($cache_file);
		} else {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://' . $host . '/shares?providers=facebook,twitter,google,linkedin,pinterest,tumblr,pocket,reddit,vk&url=' . $current_url);
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				// Windows only over-ride
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			}
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			$headers = array();
			$headers[] = 'Authorization:' . $api;
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$statistics = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			file_put_contents($cache_file, $statistics, LOCK_EX);
		}

		$json = json_decode($statistics);

		if (isset($json->shares)) {
			return $json;
		}

		return array();
	}

	public static function getTemplate()
	{

		$current_url = JFactory::getURI()->toString();
		$page_title = JFactory::getDocument()->getTitle();

		$output = '
			<#
				let current_url = "' . $current_url . '"
				let page_title = "' . $page_title . '"
				let show_totalshare = data.show_totalshare || ""
				let share_col = "jwpf-col-sm-12"
				let icons_col = "jwpf-col-sm-12"
				let totalShareText = "' . JText::_("COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_TOTAL_SHARES") . '"

				let sShareClass = data.class || ""
						sShareClass += (!_.isEmpty(data.social_style))? " jwpf-social-share-style-"+data.social_style.replace("_","-"):""

				if (show_totalshare == 1) {
					share_col = "jwpf-col-sm-3"
					icons_col = "jwpf-col-sm-9"
				}
			#>

			<style type="text/css">
				#jwpf-addon-{{ data.id }} .jwpf-social-share-wrap ul li a {
					<# if( data.background_color && data.social_style == "custom" ) {#>
					background-color: {{ data.background_color }};
					<# } #>
					<# if( data.icon_color && data.social_style == "custom" ) {#>
						color: {{ data.icon_color }};
					<# } #>
					<# if( data.social_border_width && data.social_use_border ){ #>
						border-style: solid;
						border-width: {{ data.social_border_width }}px;
					<# } #>
					<# if( data.social_border_color && data.social_use_border ){ #>
						border-color: {{ data.social_border_color }};
					<# } #>
					<# if( data.social_border_radius ){ #>
						border-radius: {{ data.social_border_radius }};
					<# } #>
				}

				#jwpf-addon-{{ data.id }}  .jwpf-social-share-wrap ul li a:hover {
					<# if( data.background_hover_color && data.social_style == "custom" ) { #>
						background-color: {{ data.background_hover_color }};
					<# } #>
					<# if( data.icon_hover_color && data.social_style == "custom" ) { #>
						color: {{ data.icon_hover_color }};
					<# } #>
					<# if( data.social_border_hover_color && data.social_use_border ){ #>
						border-color: {{ data.social_border_hover_color }};
					<# } #>
				}

				<# if (data.icon_align) { #>
					#jwpf-addon-{{ data.id }} .jwpf-social-share-wrap{
						text-align: {{ data.icon_align }};
					}
				<# } #>

			</style>

			<# if(!data.api){ #>
				<p class="alert alert-warning">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_API_NOT_FOUND') . '</p>
			<# } #>

			<div class="jwpf-addon jwpf-addon-social-share {{ sShareClass }}">
				<div class="jwpf-social-share">
					<# if( !_.isEmpty( data.title ) ){ #><{{ data.heading_selector }} class="jwpf-addon-title jw-inline-editable-element" data-id={{data.id}} data-fieldName="title" contenteditable="true">{{{ data.title }}}</{{ data.heading_selector }}><# } #>
					<div class="jwpf-social-share-wrap jwpf-row">

					<# if(show_totalshare){ #>
						<div class="jwpf-social-total-shares {{ share_col }}">
							<em>000</em>
							<div class="jwpf-social-total-share-caption">{{ totalShareText }}</div>
						</div>
					<# } #>

					<div class="jwpf-social-items-wrap {{ icons_col }}">
						<ul>
						<# if(_.indexOf(data.show_socials, "facebook") != "-1") { #>
							<li class="jwpf-social-share-facebook">
							  <a onClick="window.open(\'http://www.facebook.com/sharer.php?u={{ current_url }}\',\'Facebook\',\'width=600,height=300,left=\'+(screen.availWidth/2-300)+\',top=\'+(screen.availHeight/2-150)+\'\'); return false;" href="http://www.facebook.com/sharer.php?u={{current_url}}">
								  <i class="fa fa-facebook"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_FACEBOOK') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "twitter") != "-1") { #>
							<li class="jwpf-social-share-twitter">
							  <a onClick="window.open(\'http://twitter.com/share?url={{ current_url }}&amp;text={{ page_title }}\',\'Twitter share\',\'width=600,height=300,left=\'+(screen.availWidth/2-300)+\',top=\'+(screen.availHeight/2-150)+\'\'); return false;" href="http://twitter.com/share?url={{ current_url }}&amp;text={{ page_title }}">
								  <i class="fa fa-twitter"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_TWITTER') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "gplus") != "-1") { #>
							<li class="jwpf-social-share-glpus">
							  <a onClick="window.open(\'https://plus.google.com/share?url={{ current_url }}\',\'Google plus\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="https://plus.google.com/share?url={{ current_url }}" >
								  <i class="fa fa-google-plus"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_GOOGLE_PLUS') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "linkedin") != "-1") { #>
							<li class="jwpf-social-share-linkedin">
							  <a onClick="window.open(\'http://www.linkedin.com/shareArticle?mini=true&url={{ current_url }}\',\'Linkedin\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url={{ current_url }}">
								  <i class="fa fa-linkedin-square"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_LINKEDIN') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "pinterest") != "-1") { #>
							<li class="jwpf-social-share-pinterest">
							  <a onClick="window.open(\'http://pinterest.com/pin/create/button/?url={{ current_url }}&amp;description={{ page_title }}\',\'Pinterest\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://pinterest.com/pin/create/button/?url={{ current_url }}&amp;description={{ page_title }}" >
								  <i class="fa fa-pinterest"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_PINTEREST') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "thumblr") != "-1") { #>
							<li class="jwpf-social-share-thumblr">
							  <a onClick="window.open(\'http://tumblr.com/share?s=&amp;v=3&amp;t={{ page_title }}&amp;u={{ current_url }}\',\'Thumblr\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://tumblr.com/share?s=&amp;v=3&amp;t={{ page_title }}&amp;u={{ current_url }}" >
								  <i class="fa fa-tumblr"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_THUMBLR') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "getpocket") != "-1") { #>
							<li class="jwpf-social-share-getpocket">
							  <a onClick="window.open(\'https://getpocket.com/save?url={{ current_url }}\',\'Getpocket\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="https://getpocket.com/save?url={{ current_url }}" >
								  <i class="fa fa-get-pocket"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_GETPOCKET') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "reddit") != "-1") { #>
							<li class="jwpf-social-share-reddit">
							  <a onClick="window.open(\'http://www.reddit.com/submit?url={{ current_url }}\',\'Reddit\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://www.reddit.com/submit?url={{ current_url }}">
								  <i class="fa fa-reddit"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_REDDIT') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "vk") != "-1") { #>
							<li class="jwpf-social-share-vk">
							  <a onClick="window.open(\'http://vk.com/share.php?url={{ current_url }}\',\'Vk\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="http://vk.com/share.php?url={{ current_url }}" >
								  <i class="fa fa-vk"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_VK') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "xing") != "-1") { #>
							<li class="jwpf-social-share-xing">
							  <a onClick="window.open(\'https://www.xing.com/spi/shares/new?cb=0&url={{ current_url }}\',\'Xing\',\'width=585,height=666,left=\'+(screen.availWidth/2-292)+\',top=\'+(screen.availHeight/2-333)+\'\'); return false;" href="https://www.xing.com/spi/shares/new?cb=0&url={{ current_url }}">
								  <i class="fa fa-xing"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_XING') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>

						<# if(_.indexOf(data.show_socials, "whatsapp") != "-1") { #>
							<li class="jwpf-social-share-whatsapp">
							  <a href="whatsapp://send?text={{ current_url }}">
								  <i class="fa fa-whatsapp"></i>
									<# if ( data.show_social_names != 0 ) { #>
										<span class="jwpf-social-share-title">' . JText::_('COM_JWPAGEFACTORY_ADDON_SOCIALSHARE_WHATSAPP') . '</span>
									<# } #>
									<# if ( data.show_counter ) { #>
										<span class="jwpf-social-share-count">00</span>
									<# } #>
							  </a>
						  </li>
						<# } #>
						</ul>
					</div>

					</div>
				</div>
			</div>
			';

		return $output;
	}

}
